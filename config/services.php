<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],


    'facebook' => [ 
        'client_id' => '467829866933735',
        'client_secret' => '4b13c868ab0057e3cfbd2d14cdb3869e',
        'redirect' => 'http://localhost:8000/auth/facebook/callback',
    ],

    

    'google' => [ 
        'client_id' => '228366673771-8mtdmpd8bk2b92kqt3tgiilkip8rsugr.apps.googleusercontent.com',
        'client_secret' => 'GDZ1O6BmillJ_V9C4QPzPcAk',
        'redirect' => 'http://localhost:8000/auth/google/callback',
    ],

];
