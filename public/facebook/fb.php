<?php


	# Start the session
	session_start();

	# Autoload the required files
	require_once 'vendor/autoload.php';

	# Set the default parameters
	$fb = new Facebook\Facebook([
	  'app_id' => '118819628804907',
	  'app_secret' => '9e702b92ec2c6b0ba42812b4ddcf9fdd',
	  'default_graph_version' => 'v2.5',
	]);
	$redirect = 'http://localhost/gis/facebook/index.php';


	# Create the login helper object
	$helper = $fb->getRedirectLoginHelper();

	# Get the access token and catch the exceptions if any
	try {
	  $accessToken = $helper->getAccessToken();
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
	  // When Graph returns an error
	  echo 'Graph returned an error: ' . $e->getMessage();
	  exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
	  // When validation fails or other local issues
	  echo 'Facebook SDK returned an error: ' . $e->getMessage();
	  exit;
	}


	# If the
	if (isset($accessToken)) {
	  	// Logged in!
	 	// Now you can redirect to another page and use the
  		// access token from $_SESSION['facebook_access_token']
  		// But we shall we the same page

		// Sets the default fallback access token so
		// we don't have to pass it to each request
		$fb->setDefaultAccessToken($accessToken);

		try {
		  $response = $fb->get('/me?fields=email,name');
		  $userNode = $response->getGraphUser();
		}catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}

		function db_select($name, $id, $email, $dpurl)
		{
			$servername = "localhost";
			$username = "root";
			$password = "";
			$dbname = "fb_sdk";

			// Create connection
			$conn = new mysqli($servername, $username, $password, $dbname);
			// Check connection
			if ($conn->connect_error) {
			    die("Connection failed: " . $conn->connect_error);
			}

			$sql = "SELECT * FROM user_data where userid = '" . $id . "' limit 1";
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
			    // output data of each row
			    while($row = $result->fetch_assoc()) {
			        if($row['userid']==$id)
							{
								header('Location: http://localhost:8000/api/auth/login?email=' . $row['email'] . '&password=' . $row['userid']);
							}
			    }
					//set session => login

					return 1;
			}
			else
			{
				if(db_insert($name, $id, $email, $dpurl)==1)
				{
					header('Location: http://localhost:8000/api/auth/register?name=' . $name . '&email=' . $email . '&password=' . $id);
					$conn->close();
					return 1;
				}
				else
				{
					echo "Error in linking account!";
				}
				return 0;
			}
			$conn->close();
		}

		function db_insert($name, $id, $email, $dpurl)
		{
				$servername = "localhost";
				$username = "root";
				$password = "";
				$dbname = "fb_sdk";

				// Create connection
				$conn = new mysqli($servername, $username, $password, $dbname);
				// Check connection
				if ($conn->connect_error) {
				    die("Connection failed: " . $conn->connect_error);
				}

				$sql = "INSERT INTO user_data (username, userid, email, dpurl)
				VALUES ('" . $name . "','" .  $id . "','" . $email . "','" .  $dpurl . "')";

				if ($conn->query($sql) === TRUE) {
					$conn->close();
				    return 1;
				} else {
					$conn->close();
				    return 0;
				}

				$conn->close();
				return 0;
		}


		// Print the user Details
		echo "Welcome !<br><br>";
		echo 'Name: ' . $userNode->getName().'<br>';
		echo 'User ID: ' . $userNode->getId().'<br>';
		echo 'Email: ' . $userNode->getProperty('email').'<br><br>';

		$image = 'https://graph.facebook.com/'.$userNode->getId().'/picture?width=200';
		echo "Picture<br>";
		echo "<img src='$image' /><br><br>";

		if(db_select($userNode->getName(),$userNode->getId(),$userNode->getProperty('email'),$image)==1)
		{
			file_get_contents("http://localhost:8000");
		}
		else
		{

		}


	}else{
		$permissions  = ['email'];
		$loginUrl = $helper->getLoginUrl($redirect,$permissions);
		if(isset($_REQUEST['fb_login']))
		{
			header("location: " . $loginUrl);
			die();
		}
		echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
	}
