<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\User;
use JWTAuthException;
class UserController extends Controller
{

    private $user;
    public function __construct(User $user){
        $this->user = $user;
    }

    public function register(Request $request){
        $user = $this->user->create([
          'name' => $request->get('name'),
          'email' => $request->get('email'),
          'password' => bcrypt($request->get('password'))
        ]);
        // return response()->json(['status'=>true,'message'=>'User created successfully','data'=>$user]);
        return "<h1>Account linked successfully</h1>Name : " . $user->name . "<br>Email ID : " . $user->email .
          "<br><hr><b style='color:red'>Please wait ... Redirecting...!</b><meta http-equiv='refresh' content='1; url=http://localhost:8000/api/auth/login?email=" . $user->email . "&password=" . $request->get('password') . "' />";
    }

    public function login(Request $request){
        $credentials = $request->only('email', 'password');
        $token = null;
        try {
           if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['invalid_email_or_password'], 422);
           }
        } catch (JWTAuthException $e) {
            return response()->json(['failed_to_create_token'], 500);
        }
        // return response()->json(compact('token'));
        return "Logged in... <meta http-equiv='refresh' content='0; url=http://localhost:8000/api/user?token=" . $token . "' />";
    }


      // Session::put('key','value');//to put the session value
      // Session::get('key');//to get the session value
      // Session::has('key');//to check the session variable exist or not

    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);

        session()->put('email', $user->email);
        return "Email ID : " . session("email") . "<br>";
        // return response()->json(['result' => $user]);
    }
}
